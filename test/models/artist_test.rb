require 'test_helper'

class ArtistTest < ActiveSupport::TestCase

  def params
    {
      brand: "Bar Artist Test",
      country: "Rissia",
      founded: 1655,
      closed: 1755
    }
  end

  test "should create valid record" do
    Artist.delete_all
    artist = Artist.new(params)

    assert artist.valid?
    artist.save
    assert_equal 1, Artist.count
    assert_equal 'Bar Artist Test', artist.brand
    assert_equal 'Rissia', artist.country
    assert_equal 1655, artist.founded
    assert_equal 1755, artist.closed
  end

  test "should not create artist without complete params" do
    len = 0
    params.except(:country, :founded, :closed).keys.each do |key|
      artist = Artist.new(params.except(key))
      artist.valid?
      len += artist.errors.messages.length
    end
    assert_equal( true, (len > 0))
  end
end
