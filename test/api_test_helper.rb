require 'ostruct'
require 'test_helper'

class TestClient
  attr_accessor :rack_test, :default_headers

  def initialize(headers={})
    self.rack_test       = ::TestClient::RackTest.new
    self.default_headers = headers
  end

  %w(get post put delete).each do |method|
    define_method(method) do |path, params={}, headers={}|
      JsonResponse.new(rack_test.send(method, path, params, default_headers.merge(headers)))
    end
  end

  class RackTest
    include Rack::Test::Methods

    def app
      Marionette::Application
    end
  end

  class JsonResponse
    attr_reader :response
    delegate :status, :headers, to: :response

    def initialize(response)
      @response = response
    end

    def body
      @body ||= begin
        @response.body.empty?? @response.body : JSON.parse(@response.body)
      end
    end

    def data(keys=nil)
      return body['data'] unless keys
      keys = keys.to_s.split('.')
      if body && body['data']
        hash = body['data']
        while keys.length > 0
          hash = hash[keys.shift]
        end
        return hash
      else
        raise StandardError.new('data is nil')
      end
    end
  end
end

class ActionDispatch::IntegrationTest
  def api_client
    @client ||= begin
      headers = {'HTTP_ACCEPT' => 'application/json'}
      TestClient.new(headers)
    end
  end
end
