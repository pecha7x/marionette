require 'api_test_helper'

class CreateDocumentTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should be able to add document to a record" do
    file = fixture_file_upload(fixture_file_path('pdf.pdf'), 'application/pdf', :binary)
    record = records(:foo)
    params = {
      access_token: users(:bar).authentication_token,
      file: {
        data: Base64.encode64(file.read),
        content_type: file.content_type,
        file_name: file.original_filename
      }
    }

    json = api_client.post "/api/v1/records/#{record.id}/documents", params

    assert_equal 201, json.status
  end


end
