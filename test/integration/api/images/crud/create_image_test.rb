require 'api_test_helper'

class CreateImageTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should be able to add image to an record" do
    image = fixture_file_upload(fixture_image_path('large_image.jpg'), 'image/png', :binary)
    record = records(:foo)
    params = {
      access_token: users(:bar).authentication_token,
      image: {
        data: Base64.encode64(image.read),
        content_type: image.content_type,
        file_name: image.original_filename
      }
    }

    json = api_client.post "/api/v1/records/#{record.id}/images", params

    assert_equal 201, json.status
  end


end
