require 'api_test_helper'

class EditImageTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should be able to edit image" do
    image = fixture_file_upload(fixture_image_path('profile_picture.jpg'), 'image/jpg', :binary)
    record = records(:foo)
    image_model = images(:foo)
    params = {
        access_token: users(:foo).authentication_token,
        image: {
            data: Base64.encode64(image.read),
            content_type: image.content_type,
            file_name: image.original_filename
        }
    }

    json = api_client.put "/api/v1/records/#{record.id}/images/#{image_model.id}", params

    assert_equal 200, json.status
  end

end
