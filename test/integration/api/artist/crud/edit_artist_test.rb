require 'api_test_helper'

class EditArtistTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user should be able to update artist" do
    artist = artists(:foo)
    params = {
      access_token: users(:bar).authentication_token,
      brand: 'Updated artist',
    }

    json = api_client.put "/api/v1/artists/#{artist.id}", params

    assert_equal 200, json.status
    assert_equal 'Updated artist', json.data(:brand)
  end

end
