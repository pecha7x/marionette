require 'api_test_helper'

class CreateArtistTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should be able to create artist" do
    params = api_artist_params.merge(
      access_token: users(:foo).authentication_token
    )

    json = api_client.post '/api/v1/artists', params

    assert_equal 200, json.body['code']
    assert_equal params[:brand], json.data('brand')
  end
end
