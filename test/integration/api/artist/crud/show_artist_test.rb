require 'api_test_helper'

class ShowArtistsTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should be able to show artist" do
    params = {
      access_token: users(:foo).authentication_token
    }

    artist = Artist.last

    json = api_client.get "/api/v1/artists/#{artist.id}", params
    assert_equal 200, json.body['code']
    assert_not_nil json.data('brand')
  end
end
