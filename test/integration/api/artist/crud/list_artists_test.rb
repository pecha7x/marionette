require 'api_test_helper'

class ListArtistsTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user should be able to see all artists" do
    params = { access_token: users(:foo).authentication_token }
    json = api_client.get "/api/v1/artists", params

    assert_equal 200, json.status
    assert_equal 3, json.data.length
  end
end
