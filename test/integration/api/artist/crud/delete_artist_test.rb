require 'api_test_helper'

class DeleteArtistTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user should be able to delete artist" do
    artist = artists(:foo)
    params = {
      access_token: users(:qux).authentication_token
    }

    json = api_client.delete "/api/v1/artists/#{artist.id}", params

    assert_equal 200, json.body['code']
  end

end

