require 'test_helper'
require 'api_test_helper'


class ExpiredSessionTest < ActionDispatch::IntegrationTest
  fixtures :all

  before do
    json = api_client.post "/api/v1/login.json", login_params_with_email

    assert_equal 201, json.status
    assert_equal 'foo@bar.com', json.data('email')
    assert_not_nil json.data('access_token')

    @access_token = json.data('access_token')
  end

  test "should allow access if user last active was lass then 120 mins" do
    json = api_client.get "/api/v1/user/profile.json", {access_token: @access_token}

    assert_equal 200, json.status
    assert_not_nil json.data('access_token')
  end

  test "should not allow access if user last active was more then 120 mins" do
    user = users(:foo)
    user.update_columns(current_sign_in_at: DateTime.now - 300.minutes)
    json = api_client.get "/api/v1/user/profile.json", {access_token: @access_token}

    assert_equal 401, json.status
    # assert_equal "Session expired. Please make re-login.", json.status

    user.update_columns(current_sign_in_at: DateTime.now)
  end
end
