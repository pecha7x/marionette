require 'test_helper'
require 'api_test_helper'


class ForgotPasswordTest < ActionDispatch::IntegrationTest
  fixtures :users

  test "should be able to reset password" do
    skip("-_-")
    user = users(:foo)
    post '/api/v1/password/forgot', email: user.email

    user.reload
    assert_equal 201, json_response(:code)
    assert_not_nil user.reset_password_token
    reset_email = ActionMailer::Base.deliveries.last
    assert_equal ["contact-staging@marionette.com"], reset_email.from
    assert_equal [user.email], reset_email.to
    assert_equal 'Reset password instructions', reset_email.subject

    token = reset_email.body.to_s.split('reset_password_token=')[1][0..19]
    i_token = (0...5).map { ('a'..'z').to_a[rand(26)] }.join

    json = api_client.post '/api/v1/password/change',
      password: '2wsx@WSX' + i_token,
      password_confirmation: '2wsx@WSX' + i_token,
      reset_password_token: token

    assert_equal 201, json_response(:code)
    assert_equal 'Password is changed successfully', json_response(:message)
    assert_equal user.email, json_response('data.email')
  end

  test "should not request password reset instruction with unregistered email" do
    post '/api/v1/password/forgot', email: 'woo@bar.com'

    assert_equal 404, json_response(:code)
  end
end
