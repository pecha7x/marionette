require 'test_helper'
require 'api_test_helper'

class RegistrationTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "New User registration via API" do
    json = api_client.post "/api/v1/register.json", signup_params

    assert_equal 201, json.status
    assert !User.last.confirmed?
  end

  test "Registering via API with already registered email" do
    User.new(email: signup_params[:email]).save(validate: false)

    json = api_client.post "/api/v1/register.json", signup_params

    assert_equal 422, json.status
  end

  test "Registering via API without complete params" do
    post '/api/v1/register.json'

    assert_response 400
  end

  test "Registering via API with bad params" do
    post '/api/v1/register.json'

    json = api_client.post "/api/v1/register.json", signup_params.merge!({password: 12345678})

    assert_equal 422, json.status
  end
end
