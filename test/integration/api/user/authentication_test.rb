require 'test_helper'
require 'api_test_helper'

class AuthenticationTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should be able to authenticate with email and password" do

    post "/api/v1/login.json", login_params_with_email

    assert_equal 201, json_response(:code)
    assert_equal 'foo@bar.com', json_response('data.email')
    assert_not_nil json_response('data.access_token')
  end

  test "should be able to authenticate with username and password" do
    json = api_client.post "/api/v1/login.json", login_params_with_username.merge!({login: "foo_fs1"})

    assert_equal 201, json.status
    assert_equal 'foo@bar.com', json.data('email')
    assert_not_nil json.data('access_token')
  end

  test "should not be able to authenticate with unregistered email" do
    post '/api/v1/login', login_params_with_email.merge!({login: 'foofoo@bar.com'})

    assert_equal 401, json_response(:code)
    assert_equal 'NotAuthorizedError', json_response(:status)
  end

  test "should not be able to authenticate with wrong password" do
    post '/api/v1/login', login_params_with_email.merge(password: '111111111')

    assert_equal 401, json_response(:code)
    assert_equal 'NotAuthorizedError', json_response(:status)
  end

  test "should not be able without params" do
    post '/api/v1/login'

    assert_equal 'login is missing, password is missing', json_response(:error)
  end
end
