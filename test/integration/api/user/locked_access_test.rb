require 'test_helper'
require 'api_test_helper'


class LockedAccessTest < ActionDispatch::IntegrationTest
  fixtures :users

  test "should not allow access if user try login more then 10 attemps" do
    user = users(:qux)
    user.unlock_access!
    10.times do
      json = api_client.post "/api/v1/login.json", {login: user.email,password: "1qaz@WSX"}

      assert_equal 'NotAuthorizedError', json.body['status']
      assert_equal 401, json.body['code']
    end

    json = api_client.post "/api/v1/login.json", {login: user.email, password: "1qaz!QAZ"}

    assert_equal 'NotAuthorizedError', json.body['status']
    assert_equal 'Account is locked please contact us', json.body['error']
    assert_equal 401, json.body['code']

    user.unlock_access!
  end
end
