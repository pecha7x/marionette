ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/reporters'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

class ActiveSupport::TestCase
  # extend Paperclip::Shoulda::Matchers
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  # fixtures :all

  # Add more helper methods to be used by all tests here...

  def json_response(keys)
    keys = keys.to_s.split('.')
    if @response && @response.body
      hash = ActiveSupport::JSON.decode(@response.body)
      while keys.length > 0
        hash = hash[keys.shift]
      end
      return hash
    end
  end

  def login_user(user)
    params = {
      email: user.email,
      password: '123123123'
    }
    post '/api/v1/login', params
    @access_token = user.authentication_token
  end

  def fixture_image_path(name)
    File.join(Rails.root, 'test', 'fixtures', 'images', name)
  end

  def fixture_file_path(name)
    File.join(Rails.root, 'test', 'fixtures', 'files', name)
  end

  def artist_params(brand="My Artist")
    {
      brand: brand,
      country: 'Rissia',
      founded: 1655,
      closed: 1755
    }
  end

  def api_artist_params(name="My Artist")
    {
      brand: name,
      country: 'Rissia',
      founded: 1655,
      closed: 1755
    }
  end

  def login_params_with_email
    {
      login: 'foo@bar.com',
      password: '1qaz!QAZ'
    }
  end

  def login_params_with_email1
    {
        login: 'qux@bar.com',
        password: '1qaz!QAZ'
    }
  end

  def login_params_with_username
    {
        login: 'andrey@lodossteam.com',
        password: '1qaz!QAZ'
    }
  end

  def signup_params
    {
      email: 'andrey@lodossteam.com',
      password: '1qaz!QAZ'
    }
  end
end

class ActionDispatch::IntegrationTest
  # For devise authentication helpers
  include Warden::Test::Helpers
  Warden.test_mode!

  teardown do
    Warden.test_reset!
    # rm_rf_uploaded_dir
  end
end
