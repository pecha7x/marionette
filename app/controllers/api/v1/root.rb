module API
  module V1
    class Root < Grape::API
      version 'v1'
      use API::Concern::ApiLogger
      include API::Concern::Status
      include API::Concern::Users

      add_swagger_documentation(
        base_path: "/api",
        hide_documentation_path: true,
        api_version: 'v1'
      )
    end
  end
end
