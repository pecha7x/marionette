require 'api_error'

module API
  module Concern
    module Users
      extend ActiveSupport::Concern
      included do
        segment :users do
          desc "return all users data"
          params do
            optional :term, type: String, desc: "The search term"
            optional :page, type: Integer, desc: 'page number for users list', default: 1
            optional :per_page, type: Integer, desc: 'specify how many users in a page, default to 25', default: 25
          end
          get do
            begin
              users = UserManager.new.latest
              users = users.search_by_brand params[:term] if params[:term].present?
              users = users.order('created_at DESC').page(page).per(per_page)
              success! users.as_api_response(:basic), 200
            rescue => e
              throw_error! 403, e.class.to_s, e.message
            end
          end

          desc "return an user data"
          get '/:id' do
            begin
              user = UserManager.new.find(params[:id])
              success! user.as_api_response(:basic), 200
            rescue => e
              throw_error! 403, e.class.to_s, e.message
            end
          end

          desc "create a new user"
          params do
            requires :name, type: String, desc: 'User Name'
            optional :country, type: String, desc: 'User country'
            optional :avatar, type: Hash do
              optional :data, type: String, desc: 'Base64-encoded image data'
              optional :file_name, type: String, desc: 'image file name'
              optional :content_type, type: String, values: ['image/jpeg', 'image/jpg', 'image/png'], desc: 'image content type, default is image/jpeg'
            end
          end
          post do
            begin
              user_params = ActionController::Parameters.new(params).permit(:name, :country, avatar: [:data, :file_name, :content_type])
              user = UserManager.new(current_user).create_user(user_params)
              success! user.as_api_response(:basic), 200
            rescue => e
              throw_error! 403, e.class.to_s, e.message
            end
          end

          desc "update exist user"
          params do
            optional :name, type: String, desc: 'User Name'
            optional :country, type: String, desc: 'User country'
            optional :avatar, type: Hash do
              optional :data, type: String, desc: 'Base64-encoded image data'
              optional :file_name, type: String, desc: 'image file name'
              optional :content_type, type: String, values: ['image/jpeg', 'image/jpg', 'image/png'], desc: 'image content type, default is image/jpeg'
            end
          end
          put ':id' do
            begin
              user_params = ActionController::Parameters.new(params).permit(:name, :country, avatar: [:data, :file_name, :content_type])
              user = UserManager.new(current_user).update_user(params[:id], user_params)
              success! user.as_api_response(:light), 200
            rescue => e
              throw_error! 403, e.class.to_s, e.message
            end
          end

          desc "delete exist user"
          params do
            optional :access_token, type: String, desc: 'User access token'
          end
          delete ':id' do
            begin
              user = UserManager.new(current_user).delete_user(params[:id])
              success! user.as_api_response(:basic), 200
            rescue => e
              throw_error! 403, e.class.to_s, e.message
            end
          end
        end
      end
    end
  end
end
