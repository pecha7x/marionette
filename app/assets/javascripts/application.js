// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require angular
//= require angular-animate
//= require angular-cookies
//= require angular-touch
//= require angular-sanitize
//= require angular-ui-router
//= require angular-bootstrap
//= require angular-rails-templates
//= require js-data
//= require js-data-http
//= require js-data-angular
//= require angular-local-storage
//= require moment
//= require lodash
//= require angular-moment
//= //require slick-carousel
//= //require angular-slick
//= require ngScrollSpy
//= require angular-base64-upload
//= require ngMask
//= require angular-elastic

// new components need include before
//= require jquery_ujs
//= require app/index
//= require_tree ./app
//= require_tree ./templates