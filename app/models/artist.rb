class Artist < ActiveRecord::Base
  include ActsAs::SharedApi

  include PgSearch
  pg_search_scope :search_by_name,
    against: [:name],
    using: {
        tsearch: { prefix: true, any_word: true }
    }

  mount_uploader :avatar, ImageUploader

  api_accessible :light do |t|
    t.add :id
  end

  api_accessible :basic do |t|
    t.add :id
    t.add :name
    t.add :country
    t.add :image_with_size, as: :avatar
  end

  def image_with_size
    {
      large:  absolute_url(self.avatar.url(:large)),
      medium: absolute_url(self.avatar.url(:medium)),
      small:  absolute_url(self.avatar.url(:small)),
    }
  end

end
