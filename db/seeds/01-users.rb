module SeedUser
  def self.seed
    User.destroy_all
    10.times do |n|
      generate_user(n)
    end
  end

  def self.generate_user(counter=0)
    image = begin
      fn = "bg#{(1..9).to_a.sample}.jpg"
      File.open(File.join(Rails.root, 'db', 'fixtures', 'images', fn))
    end

    data_user = {
      name:   Faker::Name.name + " #{counter}",
      country: Faker::Address.country,
      avatar: image
    }

    user = User.new(data_user)
    user.save
  end
end
